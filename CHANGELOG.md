__v0.5.0__

* add README.md
* add LICENSE.txt
* improve API docs

__v0.4.0__

* NULL byte(s) can be written to and read from a channel.
